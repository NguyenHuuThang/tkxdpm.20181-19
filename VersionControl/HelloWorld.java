package version_control;
import java.util.Scanner;
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Hãy nhập tên của bạn:");
		String name = scanner.nextLine();
		
		int day = 0;
		int month = 0;
		int year = 0;
		do{
		System.out.println("Hãy nhập ngày sinh của bạn:");
		day = scanner.nextInt();
		month = scanner.nextInt();
		year = scanner.nextInt();
		}while(!DateUtil.checkDate(day, month, year));
		
		System.out.println("Xin chào " + name);
		System.out.println("Số tuổi của bạn là: " + DateUtil.calculateAge(year));
		
	}

}
