package version_control;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static boolean checkDate(int day, int month, int year) {
		if (year < 0)
			return false;
		boolean laNamNhuan = (year % 4 == 0) ? true : false;
		switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return isDayValid(day, 31);
			case 4:
			case 6:
			case 9:
			case 11:
				return isDayValid(day, 30);
			case 2:
				return laNamNhuan ? isDayValid(day, 29) : isDayValid(day, 28);
			default:
				return false;
		}
	}

	private static boolean isDayValid(int day, int soNgay) {
		return day > 0 && day <= soNgay;
	}

	public static int calculateAge(int year) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();
		String currentYear = dateFormat.format(date);

		if (currentYear.isEmpty() && year == 0){
			System.out.println("Please enter your birth year");
			return 0;
		}

		if (parseInt(currentYear) < year){
			System.out.println("Please renter your birth year, you birth in future");
			return 0;
		}

		return parseInt(currentYear) - year;
	}

	public static int parseInt(String number){
		return Integer.parseInt(number);
	}
}
