package version_control;

public class NumberUtil {
	public static boolean checkPrimeNumber(int number)
	{
		int count = 0;
		for(int i = 2; i <= Math.sqrt(number); i++)
		{
			if((number % i) == 0) return false;
		}
		return true;
	}
	
	public static boolean checkSquareNumber(int n) {
		int z = (int) Math.sqrt(n);
		if (z * z == n) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkPerfectNumber(int n) {
		int divisorSum = 1;

		for (int i = 2; i <= n / 2 + 1; i++) {
			if (n % i == 0) {
				divisorSum += i;
			}
		}

		if (divisorSum == n) {
			return true;
		}
		return false;
	}

	public static boolean checkCompositeNumber ( int n){
		if (!checkPrimeNumber(n)){
			return true;
		}
		return false;
	}
	
}


