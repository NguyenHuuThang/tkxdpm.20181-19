Folder description: Chứa tất cả sequence diagram

Note:
	- Các sequence diagram cần được review bởi các thành viên trong nhóm (cái này sẽ giúp vẽ class tốt hơn)
	- Các thành viên trong nhóm phải review cho nhau, comment trực tiếp vào pull request
	- Pull request được merge cần có tối thiểu 2 approve của 2 thành viên trong nhóm không phải mình
	- Khi gửi pull request cần ping các thành viên trong nhóm và review, coppy link pull request cho các thành viên