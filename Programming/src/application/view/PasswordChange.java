package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller;
import application.controller.Controller.Options;
import application.controller.LogInController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class PasswordChange implements Initializable {

	private LogInController loginController;
	@FXML
	private TextField txtUserName;
	@FXML
	private PasswordField txtPassOld;
	@FXML
	private PasswordField txtPassNew;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		loginController = (LogInController) Main.getControllerManage().getController("LogInController");
	}

	public void Change(ActionEvent e) {
		Options options = new Options();
		options.putValue("username", txtUserName.getText().toString());
		options.putValue("password", txtPassOld.getText().toString());
		if (loginController.success(options)) {
			System.out.println("Thay đổi mật khẩu");
			Options options1 = new Options();
			options1.putValue("username", txtUserName.getText().toString());
			options1.putValue("password", txtPassNew.getText().toString());
			loginController.update(options1);
			System.out.println("Thay đổi mật khẩu thành công");
		}else {
			System.out.println("fail");
			Alert alert= new Alert(AlertType.ERROR);
			alert.setTitle("Thay đổi mật khẩu thất bại");
			alert.setHeaderText("Thay đổi thất bại");
			alert.setContentText("Nhập mật khẩu sai");
			alert.showAndWait();
		}
	}

	public void goToBackPage(ActionEvent e) throws IOException {
		loginController.ridirect("../../scences/Application.fxml", e);
	}
}
