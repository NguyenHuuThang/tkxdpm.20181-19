package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.text.TabableView;

import application.Main;
import application.controller.Controller.Options;
import application.controller.LogInController;
import application.model.UserCollection.User;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class LogIn implements Initializable {

	private LogInController loginController;

	@FXML
	private TextField txtUserName;
	@FXML
	private PasswordField txtPassword;
	@FXML
	private TableView<User> tableLogin;
	@FXML
	private TableColumn<User, String> username;
	@FXML
	private TableColumn<User, String> password;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loginController = (LogInController) Main.getControllerManage().getController("LogInController");
	}

	public void login(ActionEvent e) throws IOException {
		System.out.print("login ");
		Options options = new Options();
		options.putValue("username", txtUserName.getText().toString());
		options.putValue("password", txtPassword.getText().toString());
		if (loginController.success(options)) {
			System.out.println("success");
			loginController.ridirect("../../scences/Application.fxml", e); // các lớp kế thừa từ controller có thể sử
																			// dụng hàm này
		} else {
			System.out.println("fail");
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Đăng nhập thất bại");
			alert.setHeaderText("Đăng nhập thất bại");
			alert.setContentText("Tài khoản hoặc mật khẩu không đúng");
			alert.showAndWait();
		}
	}

	public void goToMainPage(Event e) throws IOException {
		loginController.ridirect("../../scences/Application.fxml", e);
	}
}
