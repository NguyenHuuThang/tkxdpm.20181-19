package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller.Options;
import application.controller.RegisterController;
import application.model.RegisterCollection.Register;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class RegisterStudy implements Initializable {

	private RegisterController registerController;
	@FXML
	private TextField txtMHP;
	@FXML
	private TableView<Register> tableRegister;
	@FXML
	private TableColumn<Register, String> idMHP;
	@FXML
	private TableColumn<Register, String> tcDKHP;
	@FXML
	private TableColumn<Register, String> tcDKTL;
	@FXML
	private TableColumn<Register, String> tcDKTT;
	@FXML
	private TableColumn<Register, String> tcDKSTC;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		registerController = (RegisterController) Main.getControllerManage().getController("RegisterController");
	}

	public void registerStudy(ActionEvent e) {
		System.out.println("Đăng ký học phần");
		Options options = new Options();
		options.putValue("idMHP", txtMHP.getText().toString());
		if (registerController.success(options)) {
			System.out.println("success");
			tcDKHP.setCellValueFactory(new PropertyValueFactory<Register, String>("tcDKHP"));
			tcDKTL.setCellValueFactory(new PropertyValueFactory<Register, String>("tcDKTL"));
			tcDKTT.setCellValueFactory(new PropertyValueFactory<Register, String>("tcDKTT"));
			tcDKSTC.setCellValueFactory(new PropertyValueFactory<Register, String>("tcDKSTC"));
			tableRegister.setItems(registerController.listDbRegister(options));

		} else {
			System.out.println("fail");
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Đăng ký thất bại");
			alert.setHeaderText("Đăng ký thất bại");
			alert.setContentText("Mã học phần không tồn tại");
			alert.showAndWait();
		}
	}

	public void goToMainPage(Event e) throws IOException {
		registerController.ridirect("../../scences/Application.fxml", e);
	}

}
