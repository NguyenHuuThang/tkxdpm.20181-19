package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class MainPage implements Initializable {
	
	Controller controller;
	@FXML
	private AnchorPane rootPane;
	@FXML
	private Pane contentPane;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		controller = Main.getControllerManage().getController("Controller");
	}
	
	@FXML
	public void showUserInfoPane(ActionEvent event) {
		switchPane(event, "UserInfo.fxml");
	}
	
	public void switchPane(ActionEvent event, String fxmlScene) {
		AnchorPane newContentPane;
		try {
			newContentPane = FXMLLoader.load(getClass().getResource(fxmlScene));
			List<Node> childrens = rootPane.getChildren();
			System.out.println(childrens.indexOf(contentPane) + newContentPane.toString());
			childrens.set(childrens.indexOf(contentPane), newContentPane);
			contentPane = newContentPane;
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
	}
}
