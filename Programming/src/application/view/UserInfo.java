package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller;
import application.controller.UserController;
import javafx.fxml.Initializable;

public class UserInfo implements Initializable{

	private UserController userController;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		userController = (UserController) Main.getControllerManage().getController("UserController");
	}

}
