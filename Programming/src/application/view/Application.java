package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller;
import javafx.event.Event;
import javafx.fxml.Initializable;

public class Application implements Initializable {

	private Controller controller;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		controller = Main.getControllerManage().getController("Controller");

	}

	public void goToLogIn(Event e) throws IOException {
		System.out.println(e);
		controller.ridirect("../../scences/LogIn.fxml", e);
	}

	public void goToPassChange(Event e) throws IOException {
		System.out.println(e);
		controller.ridirect("../../scences/PassWordChange.fxml", e);
	}

	public void goToViewSchedule(Event e) throws IOException {
		System.out.println(e);
		controller.ridirect("../../scences/ViewSchedule.fxml", e);
	}

	public void goToRegisterStudy(Event e) throws IOException {
		System.out.println(e);
		controller.ridirect("../../scences/RegisterStudy.fxml", e);
	}
}
