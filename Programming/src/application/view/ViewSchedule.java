package application.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import application.controller.Controller;
import application.controller.Controller.Options;
import application.controller.LogInController;
import application.controller.ScheduleController;
import application.model.ScheduleCollection.Schedule;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class ViewSchedule implements Initializable {

	
	private ScheduleController scheduleController;
	@FXML
	private TextField txtMSSV;
	@FXML
	private TableView<Schedule> tableView;
	@FXML
	private TableColumn<Schedule, String> idMSSV;
	@FXML
	private TableColumn<Schedule, String> tcTG;
	@FXML
	private TableColumn<Schedule, String> tcTH;
	@FXML
	private TableColumn<Schedule, String> tcP;
	@FXML
	private TableColumn<Schedule, String> tcML;
	@FXML
	private TableColumn<Schedule, String> tcTL;
	@FXML
	private TableColumn<Schedule, String> tcGC;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		scheduleController = (ScheduleController) Main.getControllerManage().getController("ScheduleController");
	}

	public void viewSchedule(ActionEvent e) throws IOException {
		System.out.println("Xem thời khóa biểu");
		Options options = new Options();
		options.putValue("idMSSV", txtMSSV.getText().toString());
		if (scheduleController.success(options)) {
			System.out.println("success");
			tcTG.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcTG"));
			tcTH.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcTH"));
			tcP.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcP"));
			tcML.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcML"));
			tcTL.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcTL"));
			tcGC.setCellValueFactory(new PropertyValueFactory<Schedule, String>("tcGC"));
			tableView.setItems(scheduleController.listDbSchedule(options));

		}else {
			System.out.println("fail");
			Alert alert= new Alert(AlertType.ERROR);
			alert.setTitle("Xem thời khóa biểu thất bại");
			alert.setHeaderText("Không có thời khóa biểu");
			alert.setContentText("Không có thời khóa biểu");
		    alert.showAndWait();
		}
	}

	public void goToMainPage(Event e) throws IOException {
		scheduleController.ridirect("../../scences/Application.fxml", e);
	}

}
