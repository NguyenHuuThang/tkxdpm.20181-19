package application;

import application.controller.ControllerManage;
import application.model.Database;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
	private static Database database;
	private static ControllerManage controllerManage;

	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("../scences/MainPage.fxml"));
			primaryStage.setTitle("My Application");

			primaryStage.setScene(new Scene(root));
			primaryStage.show();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		database = Database.connect();
		controllerManage = ControllerManage.initialize(database);
		launch(args);
	}

	public static ControllerManage getControllerManage() {
		return controllerManage;
	}

}
