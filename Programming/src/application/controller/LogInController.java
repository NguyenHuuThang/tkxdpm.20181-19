package application.controller;

import application.model.Database;
import application.model.UserCollection;
import application.model.UserCollection.User;
import javafx.collections.ObservableList;

public class LogInController extends Controller {
	private UserCollection USERS;
	private User user;

	public LogInController(Database database) {
		this.USERS = (UserCollection) database.getCollection("User");
	}

	public boolean success(Options options) {
		user = USERS.findOneByUserNameAndPassword(options);
		return !(user == null);
	}

	public boolean update(Options options) {
		USERS.update(options);
		return !(user == null);
	}

}
