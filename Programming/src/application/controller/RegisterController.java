package application.controller;

import application.controller.Controller.Options;
import application.model.Database;
import application.model.RegisterCollection;
import application.model.RegisterCollection.Register;
import application.model.ScheduleCollection.Schedule;
import application.model.UserCollection;
import application.model.UserCollection.User;
import javafx.collections.ObservableList;

public class RegisterController extends Controller {

	private RegisterCollection REGISTER;
	private Register register;

	public RegisterController(Database database) {
		this.REGISTER = (RegisterCollection) database.getCollection("Register");
	}

	public boolean success(Options options) {
		return REGISTER.findIdMaHP(options);
	
	}

	public ObservableList<Register> listDbRegister(Options options) {
		return REGISTER.getList(options);

	}
}
