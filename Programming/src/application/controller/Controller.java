package application.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class Controller {

	public Controller() {
	}

	public void ridirect(String url, Event e) throws IOException {
		Parent parent = FXMLLoader.load(getClass().getResource(url));
		Scene scene = new Scene(parent);
		Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
		app_stage.hide(); // optional
		app_stage.setScene(scene);
		app_stage.show();
	}

	public static class Options {
		private Map<String, Object> map;

		public Options() {
			map = new HashMap<>();
		}

		public void putValue(String key, Object value) {
			map.put(key, value);
		}

		public Object getValue(String key) {
			return map.get(key);
		}
	}
}
