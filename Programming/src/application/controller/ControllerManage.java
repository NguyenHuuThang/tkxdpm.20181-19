package application.controller;

import java.util.HashMap;
import java.util.Map;

import application.model.Database;

public class ControllerManage {
	private Map<String, Controller> controllers;

	public ControllerManage(Database database) {
		controllers = new HashMap<>();
		registry(database);
	}

	public static ControllerManage initialize(Database database) {
		return new ControllerManage(database);
	}

	public Controller getController(String key) {
		return controllers.get(key);
	}

	private void registry(Database database) {
		controllers.put("Controller", new Controller() {});
		controllers.put("LogInController", new LogInController(database));
		controllers.put("RegisterController", new RegisterController(database));
		controllers.put("ScheduleController", new ScheduleController(database));
		controllers.put("UserController", new UserController(database));
	}
}
