package application.controller;

import application.model.Database;
import application.model.ScheduleCollection;
import application.model.ScheduleCollection.Schedule;
import javafx.collections.ObservableList;

public class ScheduleController extends Controller {

	private ScheduleCollection SCHEDULE;
	private Schedule sch;

	public ScheduleController(Database database) {
		this.SCHEDULE = (ScheduleCollection) database.getCollection("Schedule");
	}

	public boolean success(Options options) {
		return SCHEDULE.findId(options);
		
	}

	public ObservableList<Schedule> listDbSchedule(Options options) {

		return SCHEDULE.getList(options);

	}

}
