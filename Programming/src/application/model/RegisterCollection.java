package application.model;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import application.controller.Controller.Options;
import application.model.ScheduleCollection.Schedule;
import application.model.UserCollection.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class RegisterCollection implements Collection {

	private MongoDatabase database;
	private MongoCollection<Register> collection;

	public RegisterCollection(MongoDatabase database) {
		this.database = database;
	}

	@Override
	public void registry() {
		// TODO Auto-generated method stub
		try {
			database.createCollection("register");

		} catch (Exception e) {
		}
	}

	@Override
	public void get() {
		// TODO Auto-generated method stub
		collection = database.getCollection("register", Register.class);
	}

	// đăng ký bằng mã HP
	public boolean findIdMaHP(Options options) {
		MongoCredential credential = MongoCredential.createCredential("nguyenductai", "sis","nguyenductai1234".toCharArray());
		ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(credential);
		ServerAddress serverAddress = new ServerAddress("ds151393.mlab.com", 51393);
		MongoClient client = new MongoClient(serverAddress, auths);
		DB db = client.getDB("sis");
		DBCollection collection = db.getCollection("register");

		
		String idMHP = (String) options.getValue("idMHP");

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("idMHP", idMHP);
		DBCursor cursor = collection.find(whereQuery);
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			return true;
		}
		return false;
	}

	// xuất thời khóa biểu trong DB
	public ObservableList<Register> getList(Options options) {
		ObservableList<Register> list1 = FXCollections.observableArrayList();
		MongoCredential credential = MongoCredential.createCredential("nguyenductai", "sis","nguyenductai1234".toCharArray());
		ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(credential);
		ServerAddress serverAddress = new ServerAddress("ds151393.mlab.com", 51393);
		MongoClient client = new MongoClient(serverAddress, auths);
		MongoDatabase database = client.getDatabase("sis");
		MongoCollection<Document> col = database.getCollection("register");

		String idMHP = (String) options.getValue("idMHP");

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("idMHP", idMHP);
		FindIterable<Document> documents = col.find(whereQuery);
		try (MongoCursor<Document> cur = documents.iterator()) {
			while (cur.hasNext()) {
				Register rgt = new Register();
				Document doc = cur.next();

				rgt.setIdMHP(doc.getString("idMHP"));
				rgt.setTcDKHP(doc.getString("tcDKHP"));
				rgt.setTcDKTL(doc.getString("tcDKTL"));
				rgt.setTcDKTT(doc.getString("tcDKTT"));
				rgt.setTcDKSTC(doc.getString("tcDKSTC"));
				list1.add(rgt);

			}
		}
		return list1;
	}
	
	public Register insert(Register register) {
		collection.insertOne(register);
		System.out.println("insert " + register.toString() + " to user collection");
		return register;
	}

	public static final class Register {

		private String idMHP;
		private String tcDKHP;
		private String tcDKTL;
		private String tcDKTT;
		private String tcDKSTC;

		public String getIdMHP() {
			return idMHP;
		}

		public void setIdMHP(String idMHP) {
			this.idMHP = idMHP;
		}

		public String getTcDKHP() {
			return tcDKHP;
		}

		public void setTcDKHP(String tcDKHP) {
			this.tcDKHP = tcDKHP;
		}

		public String getTcDKTL() {
			return tcDKTL;
		}

		public void setTcDKTL(String tcDKTL) {
			this.tcDKTL = tcDKTL;
		}

		public String getTcDKTT() {
			return tcDKTT;
		}

		public void setTcDKTT(String tcDKTT) {
			this.tcDKTT = tcDKTT;
		}

		public String getTcDKSTC() {
			return tcDKSTC;
		}

		public void setTcDKSTC(String tcDKSTC) {
			this.tcDKSTC = tcDKSTC;
		}

	}

}
