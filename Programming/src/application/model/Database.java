package application.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class Database {
	private static final String HOST = "ds151393.mlab.com";
	private static final int PORT = 51393;
	private static final String DATABASE = "sis";
	private static final String USER_NAME = "nguyenductai";
	private static final String PASSWORD = "nguyenductai1234";
	
	private MongoClient client;
	private MongoCredential credential;
	private CodecRegistry registry;
	private MongoDatabase database;
	private Map<String, Collection> mapCollection;
	
	private static final Database INSTANCE = new Database();
	
	@SuppressWarnings("deprecation")
	private Database() {
		this.credential = MongoCredential.createCredential(USER_NAME, DATABASE, PASSWORD.toCharArray());
		this.registry = fromRegistries(MongoClient.getDefaultCodecRegistry(), fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		this.client = new MongoClient(new ServerAddress(HOST, PORT), Arrays.asList(credential), MongoClientOptions.builder().codecRegistry(registry).build());
		this.database = client.getDatabase(DATABASE);
		System.out.println(credential.toString());
		System.out.println(database.toString());
		mapCollection = new HashMap<>();
		initColection();
		registerCollection();
	}

	private void registerCollection() {
		for(Map.Entry<String, Collection> entry: mapCollection.entrySet()) {
			entry.getValue().registry();
			System.out.println(entry.getKey() + " created");
			entry.getValue().get();
			System.out.println(entry.getKey() + " get successfull");
		}
	}

	public static Database connect() {
		return INSTANCE;
	}
	
	public Collection getCollection(String key) {
		return mapCollection.get(key);
	}
	
	private void initColection() {
		mapCollection.put("User", new UserCollection(this.database));
		mapCollection.put("Schedule",new ScheduleCollection(this.database));
		mapCollection.put("Register", new RegisterCollection(this.database));
	}
}
