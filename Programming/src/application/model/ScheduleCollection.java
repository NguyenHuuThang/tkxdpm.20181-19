package application.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import application.controller.Controller.Options;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ScheduleCollection implements Collection {

	private MongoDatabase database;
	private MongoCollection<Schedule> collection;

	public ScheduleCollection(MongoDatabase database) {
		this.database = database;
	}

	@Override
	public void registry() {
		// TODO Auto-generated method stub
		try {
			database.createCollection("schedule");

		} catch (Exception e) {
		}
	}

	@Override
	public void get() {
		// TODO Auto-generated method stub
		collection = database.getCollection("schedule", Schedule.class);
	}

	// tìm id của sv để xem thời khóa biểu trong DB
	public  boolean findId(Options options) {
		MongoCredential credential = MongoCredential.createCredential("nguyenductai", "sis","nguyenductai1234".toCharArray());
		ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(credential);
		ServerAddress serverAddress = new ServerAddress("ds151393.mlab.com", 51393);
		MongoClient client = new MongoClient(serverAddress, auths);
		DB db = client.getDB("sis");
		DBCollection collection = db.getCollection("schedule");

		Schedule sch = new Schedule();
		String idMSSV = (String) options.getValue("idMSSV");

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("idMSSV", idMSSV);
		DBCursor cursor = collection.find(whereQuery);
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			return true;
		}
		return false;
	

	}

	// xuất thời khóa biểu trong DB
	public ObservableList<Schedule> getList(Options options) {
		ObservableList<Schedule> list1 = FXCollections.observableArrayList();
		MongoCredential credential = MongoCredential.createCredential("nguyenductai", "sis","nguyenductai1234".toCharArray());
		ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(credential);
		ServerAddress serverAddress = new ServerAddress("ds151393.mlab.com", 51393);
		MongoClient client = new MongoClient(serverAddress, auths);
		MongoDatabase database = client.getDatabase("sis");
		MongoCollection<Document> col = database.getCollection("schedule");

		String idMSSV = (String) options.getValue("idMSSV");

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("idMSSV", idMSSV);
		FindIterable<Document> documents = col.find(whereQuery);
		try (MongoCursor<Document> cur = documents.iterator()) {
			while (cur.hasNext()) {
				Schedule sch = new Schedule();
				Document doc = cur.next();

				sch.setIdMSSV(doc.getString("idMSSV"));
				sch.setTcTG(doc.getString("tcTG"));
				sch.setTcTH(doc.getString("tcTH"));
				sch.setTcP(doc.getString("tcP"));
				sch.setTcML(doc.getString("tcML"));
				sch.setTcTL(doc.getString("tcTL"));
				sch.setTcGC(doc.getString("tcGC"));
				list1.add(sch);
			}
		}
		return list1;
	}

	public Schedule insert(Schedule schedule) {
		collection.insertOne(schedule);
		System.out.println("insert " + schedule.toString() + " to user collection");
		return schedule;
	}

	// Class này dùng để định nghĩa cho collection schedule cần có contructor mặc
	// định
	// và các phương thức get, set cho các property
	public static final class Schedule {

		private String idMSSV;
		private String tcTG;
		private String tcTH;
		private String tcP;
		private String tcML;
		private String tcTL;
		private String tcGC;

		public String getIdMSSV() {
			return idMSSV;
		}

		public void setIdMSSV(String idMSSV) {
			this.idMSSV = idMSSV;
		}

		public String getTcTG() {
			return tcTG;
		}

		public void setTcTG(String tcTG) {
			this.tcTG = tcTG;
		}

		public String getTcTH() {
			return tcTH;
		}

		public void setTcTH(String tcTH) {
			this.tcTH = tcTH;
		}

		public String getTcP() {
			return tcP;
		}

		public void setTcP(String tcP) {
			this.tcP = tcP;
		}

		public String getTcML() {
			return tcML;
		}

		public void setTcML(String tcML) {
			this.tcML = tcML;
		}

		public String getTcTL() {
			return tcTL;
		}

		public void setTcTL(String tcTL) {
			this.tcTL = tcTL;
		}

		public String getTcGC() {
			return tcGC;
		}

		public void setTcGC(String tcGC) {
			this.tcGC = tcGC;
		}

	}
}
