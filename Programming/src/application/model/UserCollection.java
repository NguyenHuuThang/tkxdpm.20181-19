package application.model;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteResult;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import application.controller.Controller.Options;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static com.mongodb.client.model.Filters.*;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.bson.Document;

public class UserCollection implements Collection {

	private MongoDatabase database;
	private MongoCollection<User> collection;

	public UserCollection(MongoDatabase database) {
		this.database = database;
	}

	@Override
	public void registry() {
		try {
			database.createCollection("user");

		} catch (Exception e) {
		}
	}

	@Override
	public void get() {
		collection = database.getCollection("user", User.class);

	}

	// tìm username và password để đăng nhập
	public User findOneByUserNameAndPassword(Options options) {
		String username = (String) options.getValue("username");
		String password = (String) options.getValue("password");
		User user = collection.find(and(eq("username", username), eq("password", password))).first();
		return user;
	}

	// tìm username và password để thay đổi mật khẩu
	public int update(Options options) {
		MongoCredential credential = MongoCredential.createCredential("nguyenductai", "sis","nguyenductai1234".toCharArray());
		ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(credential);
		ServerAddress serverAddress = new ServerAddress("ds151393.mlab.com", 51393);
		MongoClient client = new MongoClient(serverAddress, auths);
		DB db = client.getDB("sis");
		DBCollection collection = db.getCollection("user");
		DBObject whereClause = new BasicDBObject();

		String password = (String) options.getValue("password");
		String username = (String) options.getValue("username");
		DBObject values = new BasicDBObject();
		values.put("username", username);
		values.put("password", password);

		DBObject valuesWithSet = new BasicDBObject();
		valuesWithSet.put("$set", values);

		// Thực hiện việc update.
		WriteResult result = collection.update(whereClause, valuesWithSet);
		int effectedCount = result.getN();

		return effectedCount;
	}

	public User insert(User user) {
		collection.insertOne(user);
		System.out.println("insert " + user.toString() + " to user collection");
		return user;
	}

	// Class này dùng để định nghĩa cho collection user cần có contructor mặc định
	// và các phương thức get, set cho các property
	public static final class User {
		private String username;
		private String password;
		private String name;
		private String mssv;

		public User() {
		}

		public User(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public User(String username, String password, String name, String mssv) {
			this.username = username;
			this.password = password;
			this.name = name;
			this.mssv = mssv;
		}

		public String getUsername() {
			return this.username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return this.password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getMssv() {
			return mssv;
		}

		public void setMssv(String mssv) {
			this.mssv = mssv;
		}

		@Override
		public String toString() {
			return "{ username: " + this.username + " password: " + this.password + " }";
		}
	}
}
